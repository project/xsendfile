CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Background
 * Maintainers
 * Acknowledgments


INTRODUCTION
------------

The Xsendfile module enables fast transfer for private files in Drupal using
the Xsendfile (Apache) or the X-Accel-Redirect (Nginx) server module. It does
this by letting the webserver send the private file transfer, instead of
letting Drupal send the file response.

Currently regular file downloads and image derivative downloads (created by
image styles) are supported (8.x-1.x).


REQUIREMENTS
------------

This module requires no modules outside of Drupal core.

However, it does require web server functionality to be available in order to
work:

- If you use Apache, you'll need to install the Xsendfile server module
  (https://tn123.org/mod_xsendfile).
- If you Nginx, you'll need to enable the X-Accel-Redirect server module, which
  comes with Nginx
  (https://www.nginx.com/resources/wiki/start/topics/examples/x-accel/).


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit 
   https://www.drupal.org/node/1897420 for further information.


CONFIGURATION
-------------

 1. Enable private files. For information on how to do so, see 
    https://www.drupal.org/docs/8/core/modules/file/overview#s-private-file-system-settings-in-drupal-8

 2. Go to `/admin/config/media/file-system/xsendfile` and enable either **Files 
    transferred by Nginx using X-Accel-Redirect** or **Files transferred by
    Apache using xsendfile**, depending on your webserver type.

 3. For Nginx: Copy/paste the suggested config (see below), and reload Nginx.
    For Apache: install the xsendfile Apache module
    (https://tn123.org/mod_xsendfile/), copy/paste the suggested config
    (see below), and reload Apache.

    - Nginx X-Accel-Redirect and Drupal

      Below is an example configuration to use this module if you are running
      Nginx either as the main server or as reverse proxy (handling all static
      files).

      You must protect the private files directory from being accessible over
      the web. To make happen, you must set its location in your Nginx
      configuration to internal (http://wiki.nginx.org/NginxHttpCoreModule#internal).
      This guarantees that the files are accessible only by the server. No
      external access is possible.

      Here's a suggested configuration for protecting the private files
      directory in Nginx (assuming your private files directory lives at
      `/var/www/private_drupal_files`) the suggested config will be:

      location ^~ /var/www/private_drupal_files/ {
        internal;
      }

      You must also relay the `system/files/` path to Drupal in your Nginx
      config. See below for two possible configuration stanzas you can use for
      that, a basic and an advanced configuration.

      The exact configuration details will vary based on your setup, but
      if you use a configuration like the one at https://github.com/perusio/drupal-with-nginx, 
      private files handling will be already taken care of. Most configurations
      that are referenced in the groups.drupal.org Nginx group (http://groups.drupal.org/nginx)
      accommodate private files handling as well. Here's a basic
      configuration:

      location ^~ /system/files/ {
        include fastcgi_params;
        ## fastcgi_params is a file at the nginx config root that contains
        ## the 'usual' settings for FastCGI.
        include fastcgi_params;
        ## Here begins the specific part.
        fastcgi_param QUERY_STRING q=$uri;
        fastcgi_param SCRIPT_NAME /index.php;
        fastcgi_param SCRIPT_FILENAME $document_root/index.php;
        ## Adjust accordingly to your setup.
        ## This assumes UNIX sockets with php-fpm.
        fastcgi_pass unix:/var/run/php-fpm.sock;
      }

      Note that by default nginx **logs the _missing_ files** as a 404
      error. That's how it's supposed to be. The file doesn't exist. If
      you don't want that to happen then add `log_not_found off` to the
      above location stanza. Like this:

      location ^~ /system/files/ {
        ## fastcgi_params is a file at the nginx config root that contains
        ## the 'usual' settings for FastCGI.
        include fastcgi_params;
        ## Here begins the specific part.
        fastcgi_param QUERY_STRING q=$uri;
        fastcgi_param SCRIPT_NAME /index.php;
        fastcgi_param SCRIPT_FILENAME $document_root/index.php;
        ## Adjust accordingly to your setup.
        ## This assumes UNIX sockets with php-fpm.
        fastcgi_pass unix:/var/run/php-fpm.sock;

        log_not_found off;
      }

      or

      location ~* /system/files/ {
        ## fastcgi_params is a file at the nginx config root that contains
        ## the 'usual' settings for FastCGI.
        include fastcgi_params;
        ## Here begins the specific part.
        fastcgi_param QUERY_STRING q=$uri;
        fastcgi_param SCRIPT_NAME /index.php;
        fastcgi_param SCRIPT_FILENAME $document_root/index.php;
        ## Adjust accordingly to your setup.
        ## This assumes UNIX sockets with php-fpm.
        fastcgi_pass unix:/var/run/php-fpm.sock;

        log_not_found off;
      }

      depending on the situation discussed above.

      The advanced configuration makes use of a `fastcgi_private_files.conf`
      file (https://github.com/perusio/drupal-with-nginx/blob/master/fastcgi_private_files.conf)
      that specifies the proper fastcgi parameters for handling private files.

      For more information, see https://github.com/perusio/drupal-with-nginx.

      - Apache Xsendfile and Drupal

        Below is an example web server configuration if you are running Apache
        as your web server (assuming your private files directory lives at
        `/var/www/private_drupal_files`). Add the following to your virtual
        host configuration:

        <IfModule mod_xsendfile.c>
          XSendFile on
          XSendFilePath /var/www/private_drupal_files
        </IfModule>

        See https://tn123.org/mod_xsendfile for additional information.

 4. Security considerations

    To check whether your private files directory is accessible from the web, 
    you can run the following commands, using `curl`:

      curl -I <URI to private file>

    or alternatively with `wget`

      wget -S <URI to private file>

    If you have a private file named `foobar.pdf` in your private file
    directory located at `private`, when issuing:

      curl -I http://example.com/private/foobar.pdf

    you should get a `404 File Not Found` status
    code. If you don't, check your web server config for blocking direct access
    to the private files as described above.


BACKGROUND
----------

Lighty X-Sendfile (http://blog.lighttpd.net/articles/2006/07/02/x-sendfile) was
the first implementation of a mechanism for serving private files fast. By
private files we mean files that require some type of access control.

The mechanism the Lighty team came up with consists of a response header
(`X-Sendfile` in lighty's case) that is sent from the backend and that includes
the _real_ file path. When this header is present in the response, the server
discards the backend file response and serves the file directly.


MAINTAINERS
-----------

- brunodbo: https://www.drupal.org/u/brunodbo
- damien_vancouver: https://www.drupal.org/u/damien_vancouver
- marcoka: https://www.drupal.org/u/marcoka


ACKNOWLEDGMENTS
---------------

Thanks to the author of the xsend module (http://drupal.org/project/xsend). A
lot of code in earlier versions of this module was based on that module.

Thanks also to the people in the Nginx Drupal group (http://groups.drupal.org/nginx),
in particular for the discussion on how to adapt the `xsend` module, which
is for Apache, to be used by Nginx (http://groups.drupal.org/node/36892).
