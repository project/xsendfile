<?php

namespace Drupal\xsendfile\Controller;

use Drupal\Core\Site\Settings;
use Drupal\Core\StreamWrapper\StreamWrapperManagerInterface;
use Drupal\system\FileDownloadController;
use Drupal\xsendfile\FileDownloadResponseHeadersInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * System file controller.
 */
class XsendfileFileDownloadController extends FileDownloadController {

  /**
   * The file download response service.
   *
   * @var Drupal\xsendfile\FileDownloadResponseHeadersInterface
   */
  protected $fileDownloadHeaders;

  /**
   * FileDownloadController constructor.
   *
   * @param Drupal\Core\StreamWrapper\StreamWrapperManagerInterface|null $stream_wrapper_manager
   *   The stream wrapper manager.
   * @param Drupal\xsendfile\FileDownloadResponseHeadersInterface $file_download_headers
   *   The file download response service.
   */
  public function __construct(StreamWrapperManagerInterface $stream_wrapper_manager, FileDownloadResponseHeadersInterface $file_download_headers) {
    parent::__construct($stream_wrapper_manager);
    $this->fileDownloadHeaders = $file_download_headers;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('stream_wrapper_manager'),
      $container->get('xsendfile.file_download_response_headers')
    );
  }

  /**
   * Handles private file transfers.
   *
   * Call modules that implement hook_file_download() to find out if a file is
   * accessible and what headers it should be transferred with. If one or more
   * modules returned headers the download will start with the returned headers.
   * If a module returns -1 an AccessDeniedHttpException will be thrown. If the
   * file exists but no modules responded an AccessDeniedHttpException will be
   * thrown. If the file does not exist a NotFoundHttpException will be thrown.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request object.
   * @param string $scheme
   *   The file scheme, defaults to 'private'.
   *
   * @return \Symfony\Component\HttpFoundation\BinaryFileResponse|\Symfony\Component\HttpFoundation\Response
   *   The transferred file as response (when using PHP to transfer the private
   *   file), or a regular response, with the file path in the X-Sendfile
   *   (Apache) or X-Accel-Redirect (Nginx) header.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
   *   Thrown when the requested file does not exist.
   * @throws \Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException
   *   Thrown when the user does not have access to the file.
   *
   * @see hook_file_download()
   */
  public function download(Request $request, $scheme = 'private') {
    $target = $request->query->get('file');
    // Merge remaining path arguments into relative file path.
    $uri = $this->streamWrapperManager->normalizeUri($scheme . '://' . $target);

    if ($this->streamWrapperManager->isValidScheme($scheme) && is_file($uri)) {
      // Let other modules provide headers and controls access to the file.
      $headers = $this->moduleHandler()->invokeAll('file_download', [$uri]);

      foreach ($headers as $result) {
        if ($result == -1) {
          throw new AccessDeniedHttpException();
        }
      }

      if (count($headers)) {
        $download_method = $this->config('xsendfile.settings')->get('xsendfile_transfer');
        if ($download_method == $this->fileDownloadHeaders::XSENDFILE_PHP_TRANSFER) {
          // \Drupal\Core\EventSubscriber\FinishResponseSubscriber::onRespond()
          // sets response as not cacheable if the Cache-Control header is not
          // already modified. We pass in FALSE for non-private schemes for the
          // $public parameter to make sure we don't change the headers.
          return new BinaryFileResponse($uri, 200, $headers, $scheme !== 'private');
        }
        else {
          $file_path = base_path() . Settings::get('file_private_path') . '/' . $target;
          $xsendfile_headers = $this->fileDownloadHeaders->getHeaders($file_path);
          $headers += $xsendfile_headers;

          return new Response('', 200, $headers);
        }
      }

      throw new AccessDeniedHttpException();
    }

    throw new NotFoundHttpException();
  }

}
