<?php

namespace Drupal\xsendfile;

/**
 * Handle headers for private file downloads.
 */
interface FileDownloadResponseHeadersInterface {

  /**
   * Constants to define the file transfer methods.
   */
  const XSENDFILE_PHP_TRANSFER = 0;
  const XSENDFILE_NGINX_TRANSFER = 1;
  const XSENDFILE_APACHE_TRANSFER = 2;

  /**
   * Generate appropriate headers to handle private file downloads.
   *
   * @param string $file_path
   *   The file path on the server.
   *
   * @return array
   *   The headers to use in the file download response.
   */
  public function getHeaders(string $file_path);

}
