<?php

namespace Drupal\xsendfile;

use Drupal\Core\Config\ConfigFactoryInterface;

/**
 * Handle headers for private file downloads.
 */
class FileDownloadResponseHeaders implements FileDownloadResponseHeadersInterface {

  /**
   * The config factory.
   *
   * @var Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $config;

  /**
   * FileDownloadResponse constructor.
   *
   * @param Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->config = $config_factory->get('xsendfile.settings');
  }

  /**
   * {@inheritdoc}
   */
  public function getHeaders(string $file_path) {
    $headers = [];
    $download_method = $this->config->get('xsendfile_transfer');

    if ($download_method == self::XSENDFILE_APACHE_TRANSFER) {
      $headers['X-Sendfile'] = $file_path;
    }
    elseif ($download_method == self::XSENDFILE_NGINX_TRANSFER) {
      $headers['X-Accel-Redirect'] = $file_path;
    }

    return $headers;
  }

}
