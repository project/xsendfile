<?php

namespace Drupal\xsendfile\Form;

use Drupal\xsendfile\FileDownloadResponseHeadersInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Defines a form that configures Xsendfile.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'xsendfile.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'xsendfile_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('xsendfile.settings');

    $form['xsendfile']['xsendfile_transfer'] = [
      '#type' => 'radios',
      '#title' => 'Private files download method',
      '#default_value' => $config->get('xsendfile_transfer'),
      '#options' => [
        FileDownloadResponseHeadersInterface::XSENDFILE_PHP_TRANSFER => $this->t('Files transferred by PHP (default)'),
        FileDownloadResponseHeadersInterface::XSENDFILE_NGINX_TRANSFER => $this->t('Files transferred by Nginx using X-Accel-Redirect'),
        FileDownloadResponseHeadersInterface::XSENDFILE_APACHE_TRANSFER => $this->t('Files transferred by Apache using xsendfile'),
      ],
      '#description' => $this->t('Choose a method to transfer private files. See the <a href=":help">help page</a> for more information, or the README.md file for instructions and suggested web server configuration.', [':help' => Url::fromRoute('help.page', ['name' => 'xsendfile'])->toString()]),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('xsendfile.settings')
      ->set('xsendfile_transfer', $form_state->getValues()['xsendfile_transfer'])
      ->save();

    parent::submitForm($form, $form_state);
  }

}
