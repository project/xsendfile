<?php

namespace Drupal\xsendfile\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Drupal\Core\Routing\RoutingEvents;
use Symfony\Component\Routing\RouteCollection;

/**
 * Listens to the dynamic route events.
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {
    // @todo Do we need to change the controller on both routes? The D7 module only altered
    // the `system.files` route.
    if ($route = $collection->get('system.files')) {
      $route->setDefault('_controller', 'Drupal\xsendfile\Controller\XsendfileFileDownloadController::download');
    }
    if ($route = $collection->get('system.private_file_download')) {
      $route->setDefault('_controller', 'Drupal\xsendfile\Controller\XsendfileFileDownloadController::download');
    }
    if ($route = $collection->get('image.style_private')) {
      $route->setDefault('_controller', 'Drupal\xsendfile\Controller\XsendfileImageStyleDownloadController::deliver');
    }
  }

}
